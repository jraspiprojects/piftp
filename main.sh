#/bin/bash
# piftp
# an open-source solution for ftp server on raspberry-pi
#Thanks to https://gist.github.com/yuikns/d4967713693bef2b6423c89ddd3d155d 
# for a ftp config file which i'll edit
# More will be in the discription
#This assummes you do something like:
# curl -sSL jraspiprojects.github.io/piftp/install/ | sudo bash
# Yes, we did pipe to sudo bash
# Now, we'll start
apt-get install vsftpd wget ftp -y
cd /etc/
mv vsftpd.conf vsftpd.conf.old
wget https://raw.githubusercontent.com/jraspiprojects/piftp/master/vsftpd.conf -O vsftpd.conf
#echo "If you make new users on this machine, make sure to use piftpadd [username] to add them"
#This actually doesn't work so you have to manually add them in yourself.
#But we'll add the current user in
whoami > vsftpd.chroot_list
systemctl restart vsftpd
echo "WE ARE DONE!!! YAY!!"
echo "Connect: ftp" + $(hostname -I)
echo "You know, one of those adresses..."

